from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30, null=True, blank=True)
    # TODO Implement missing attributes in Friend model
    npm = models.CharField(max_length=10, null=True, blank=True)
    DOB = models.CharField(max_length=15, null=True, blank=True)
