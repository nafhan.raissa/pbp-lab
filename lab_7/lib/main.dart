import 'package:flutter/material.dart';
import 'package:lab_7/NavBar.dart';
import 'package:lab_7/button_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CaseWorqer',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.teal,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  //const MyHomePage({Key? key, required this.title}) : super(key: key);

  //final String title;

  @override
  _HomeState createState() => _HomeState();
}

//Referensi: https://www.youtube.com/watch?v=2rn3XbBijy4
class _HomeState extends State<Home> {
  final formKey = GlobalKey<FormState>();
  String pekerjaan = '';
  String perusahaan = '';
  String lokasi = '';
  String tipe = '';
  String tentang = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavBar(),
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'CaseWorqer',
            style: TextStyle(
                //color: Color(0xF2404000),
                fontWeight: FontWeight.bold,
                fontSize: 30),
          ),
        ),
        body: Form(
            key: formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                Text(
                  'Buka Lowongan Kerja',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                buildPekerjaan(),
                const SizedBox(height: 24),
                buildPerusahaan(),
                const SizedBox(height: 24),
                buildLokasi(),
                const SizedBox(height: 24),
                buildTipe(),
                const SizedBox(height: 24),
                buildTentang(),
                const SizedBox(height: 24),
                buildSubmit(),
              ],
            )));
  }

  Widget buildPekerjaan() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Pekerjaan',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.length < 2) {
            return 'Masukan Setidaknya 1 Karakter';
          } else {
            return null;
          }
        },
        onChanged: (value) => setState(() => pekerjaan = value),
      );

  Widget buildPerusahaan() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Perusahaan',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.length < 2) {
            return 'Masukan Setidaknya 1 Karakter';
          } else {
            return null;
          }
        },
        onChanged: (value) => setState(() => perusahaan = value),
      );

  Widget buildLokasi() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Lokasi',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.length < 2) {
            return 'Masukan Setidaknya 1 Karakter';
          } else {
            return null;
          }
        },
        onChanged: (value) => setState(() => lokasi = value),
      );

  Widget buildTipe() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Tipe',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.length < 2) {
            return 'Masukan Setidaknya 1 Karakter';
          } else {
            return null;
          }
        },
        onChanged: (value) => setState(() => tipe = value),
      );

  Widget buildTentang() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Tentang',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.length < 2) {
            return 'Masukan Setidaknya 1 Karakter';
          } else {
            return null;
          }
        },
        onChanged: (value) => setState(() => tentang = value),
      );

  Widget buildSubmit() => ButtonWidget(
        text: 'Submit',
        onClicked: () {},
      );
}
