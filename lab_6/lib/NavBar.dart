// ignore: file_names
// ignore: file_names
import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
              accountName: Text(''), accountEmail: Text('')),
          ListTile(
            title: Text('Masuk'),
            onTap: () => null,
          ),
          ListTile(
            title: Text('Daftar'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            title: Text('Home'),
            onTap: () => null,
          ),
          ListTile(
            title: Text('Cari Lowongan'),
            onTap: () => null,
          ),
          ListTile(
            title: Text('Buka Lowongan'),
            onTap: () => null,
          ),
          ListTile(
            title: Text('Tips Karier'),
            onTap: () => null,
          ),
          ListTile(
            title: Text('Forum'),
            onTap: () => null,
          ),
          ListTile(
            title: Text('Company Review'),
            onTap: () => null,
          ),
        ],
      ),
    );
  }
}
