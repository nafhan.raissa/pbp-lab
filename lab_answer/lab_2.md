Apakah perbedaan antara JSON dan XML?
-JSON hanya merupakan format data sedangkan XML adalah bahasa markup
-JSON merupakan format pertukaran data ringan, XML untuk menyimpan dan menyangkut data dari satu aplikasi ke aplikasi lain
-JSON berorientasi pada data, sedangkan XML berorientasi pada dokumen

Apakah perbedaan antara HTML dan XML?
-HTML fokus kepada penyajian data, sedangkan XML fokus kepada transfer data
-HTML adalah bahasa untuk membuat halaman web dan aplikasi web, XML adalah bahasa yang mendefinisikan aturan menyandikan dokumen dalam format yang dapat dibaca manusia dan mesin
-HTML tidak terlalu ketat pada tag penutup, sedangkan XML sangat ketat dalam memperhatikan tag penutup
-HTML memiliki tag yang sedikit terbatas, sedangkan Tag XML biasanya dapat dikembangkan kembali