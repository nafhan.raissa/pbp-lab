from django.db import models

# Create your models here.
class Note(models.Model):
    noteTo = models.CharField(max_length=20, null=True, blank=True)
    noteFrom = models.CharField(max_length=20, null=True, blank=True)
    noteTitle = models.CharField(max_length=25, null=True, blank=True)
    noteMessage = models.CharField(max_length=50, null=True, blank=True)